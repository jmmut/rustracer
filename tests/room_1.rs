use cgmath::InnerSpace;

use rustracer::out::save_to_png;
use rustracer::scene::intersectable::Color;
use rustracer::scene::{
    render, Camera, Intersectable, PixelCenterSampler, Point, Scene, Sphere, Vector,
};

mod example_scenes;
use crate::example_scenes::create_lights;

#[test]
#[ignore]
fn room_1() {
    let scene = create_scene();
    let frame = render(scene);
    save_to_png(frame, "tests/resources/room_1.png");
}

fn create_scene() -> Scene {
    let mut scene = Scene {
        camera: create_camera(),
        objects: create_objects(),
        lighting: create_lights(),
        sampler: Box::new(PixelCenterSampler {}),
    };
    scene.lighting.reflection_iterations = 0;
    scene
}

fn create_camera() -> Camera {
    let mut direction = Vector::new(-4.0, 0.0, -5.0);
    direction = direction.normalize();

    let half_height_up = Vector::new(0.0, 1.0, 0.0);
    let half_width_right = direction.cross(half_height_up);
    // direction *= 5.0;
    let camera = Camera {
        position: Point::new(21.0, 0.0, 50.0),
        direction,
        half_width_right,
        half_height_up,
        pixels_width: 1000,
        pixels_height: 1000,
    };
    camera
}

fn create_objects() -> Vec<Box<dyn Intersectable>> {
    let wall_color = Color {
        r: 250,
        g: 200,
        b: 150,
        a: 255,
    };
    let floor_color = Color {
        r: 60,
        g: 100,
        b: 160,
        a: 255,
    };
    let ceiling_color = Color {
        r: 250,
        g: 250,
        b: 250,
        a: 255,
    };
    vec![
        // Box::new(Sphere {
        //     position: Point::new(0.0, 0.0, 0.0),
        //     radius: 3.0,
        //     color: Color {
        //         r: 40,
        //         g: 100,
        //         b: 250,
        //         a: 255,
        //     },
        // }),
        Box::new(Sphere {
            position: Point::new(-310.0, 0.0, 0.0),
            radius: 300.0,
            color: wall_color,
            reflectiveness: 0.5,
        }),
        Box::new(Sphere {
            position: Point::new(0.0, 0.0, -310.0),
            radius: 300.0,
            color: wall_color,
            reflectiveness: 0.5,
        }),
        Box::new(Sphere {
            position: Point::new(0.0, -320.0, 0.0),
            radius: 300.0,
            color: floor_color,
            reflectiveness: 0.5,
        }),
        Box::new(Sphere {
            position: Point::new(0.0, 320.0, 0.0),
            radius: 300.0,
            color: ceiling_color,
            reflectiveness: 0.5,
        }),
    ]
}
