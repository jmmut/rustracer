mod example_scenes;

mod unit {
    mod test_scene {
        mod test_camera;
        mod test_intersection;
        mod test_lighting;
        mod test_png;
        mod test_raytrace;
    }
}
