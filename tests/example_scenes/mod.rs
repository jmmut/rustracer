use rustracer::scene::{Color, Intersectable, Light, Point, SceneLighting, Sphere};

#[allow(unused)]
pub fn create_sphere_scene() -> Vec<Box<dyn Intersectable>> {
    let sphere = create_sphere();
    vec![Box::new(sphere)]
}

pub fn create_sphere() -> Sphere {
    let sphere = Sphere {
        position: Point::new(10.0, 20.0, 0.0),
        radius: 3.0,
        color: Color {
            r: 40,
            g: 100,
            b: 250,
            a: 255,
        },
        reflectiveness: 0.5,
    };
    sphere
}

#[allow(unused)]
pub fn create_lights() -> SceneLighting {
    SceneLighting {
        lights: vec![
            Light {
                position: Point::new(25.0, 5.0, 0.0),
                color: Color {
                    r: 240,
                    g: 220,
                    b: 160,
                    a: 255,
                },
            },
            Light {
                position: Point::new(-5.0, 0.0, 30.0),
                color: Color {
                    r: 60,
                    g: 60,
                    b: 60,
                    a: 255,
                },
            },
        ],
        background_color: Color {
            r: 40,
            g: 40,
            b: 40,
            a: 255,
        },
        reflection_iterations: 4,
    }
}
