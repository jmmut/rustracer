use rustracer::out::save_to_png_file;
use rustracer::scene::intersectable::Color;
use rustracer::scene::Frame;

#[test]
fn save_basic_png() {
    let mut generated = tempfile::tempfile().unwrap();

    // First pixel is red and second pixel is black.
    let pixels = Vec::from([
        Color {
            r: 255,
            g: 0,
            b: 0,
            a: 255,
        },
        Color {
            r: 0,
            g: 0,
            b: 0,
            a: 255,
        },
    ]);
    let frame = Frame {
        pixels,
        width: 2,
        height: 1,
    };

    save_to_png_file(frame, &generated);
    assert_compare_files(&mut generated, "tests/resources/basic_image.png");
}

fn assert_compare_files(path_1: &mut std::fs::File, path_2: &str) {
    use std::io::Read;
    use std::io::Seek;
    let mut bytes_1 = Vec::new();
    path_1.rewind().unwrap();
    path_1.read_to_end(&mut bytes_1).unwrap();
    let bytes_2 = std::fs::read(std::path::Path::new(path_2)).unwrap();
    assert_eq!(bytes_1, bytes_2);
}
