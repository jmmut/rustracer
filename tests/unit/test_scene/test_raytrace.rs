use cgmath::InnerSpace;
use rustracer::scene::lighting::Coloring;
use rustracer::scene::raytrace::{intersect_ray_with_objects, reachable_lights};
use rustracer::scene::{Color, Direction, Intersectable, Light, Point, Ray, Sphere, SurfacePoint};

use crate::example_scenes::create_sphere;

const BACKGROUND_COLOR: Color = Color {
    r: 40,
    g: 40,
    b: 40,
    a: 255,
};

const LIGHTS: [Light; 1] = [Light {
    position: Point::new(8.0, 25.0, 32.0),
    color: Color {
        r: 200,
        g: 240,
        b: 150,
        a: 255,
    },
}];

struct MockColoring;

impl Coloring for MockColoring {
    fn compute_color(surface_point: &SurfacePoint, _lights: &[Light]) -> Color {
        surface_point.color
    }

    fn compute_reflection(
        _surface_color: Color,
        bounced_ray_color: Color,
        _reflectiveness: f32,
    ) -> Color {
        bounced_ray_color
    }
}

#[test]
fn basic_ray_trace() {
    let sphere = create_sphere();
    let expected_color = sphere.color;
    let ray = Ray {
        position: sphere.position + Point::new(0.0, 0.0, 2.0 * sphere.radius),
        direction: Direction::new(0.0, 0.0, -1.0),
    };
    let objects: Vec<Box<dyn Intersectable>> = vec![Box::new(sphere)];
    let pixel =
        intersect_ray_with_objects::<MockColoring>(&objects, &LIGHTS, BACKGROUND_COLOR, &ray, 0);
    assert_eq!(pixel, expected_color);
}

#[test]
fn bouncing_ray_trace() {
    let blue_sphere = Sphere {
        position: Point::new(10.0, 20.0, 0.0),
        radius: 3.0,
        color: Color {
            r: 0,
            g: 0,
            b: 250,
            a: 255,
        },
        reflectiveness: 0.5,
    };
    let red_sphere = Sphere {
        position: Point::new(10.0, 20.0, 30.0),
        radius: 3.0,
        color: Color {
            r: 250,
            g: 0,
            b: 0,
            a: 255,
        },
        reflectiveness: 0.5,
    };
    let expected_color = red_sphere.color;
    let ray = Ray {
        position: blue_sphere.position + Point::new(0.0, 0.0, 2.0 * blue_sphere.radius),
        direction: Direction::new(0.0, 0.0, -1.0),
    };
    let objects: Vec<Box<dyn Intersectable>> = vec![Box::new(blue_sphere), Box::new(red_sphere)];
    let pixel =
        intersect_ray_with_objects::<MockColoring>(&objects, &LIGHTS, BACKGROUND_COLOR, &ray, 1);
    assert_eq!(pixel, expected_color);
}

#[test]
fn filtering_reachable_lights() {
    const IRRELEVANT_COLOR: Color = Color {
        r: 200,
        g: 0,
        b: 0,
        a: 255,
    };
    const IRRELEVANT_REFLECTIVENESS: f32 = 0.5;

    const LIGHTS: [Light; 2] = [
        Light {
            position: Point::new(10.0, 0.0, 0.0),
            color: IRRELEVANT_COLOR,
        },
        Light {
            position: Point::new(0.0, 10.0, 0.0),
            color: IRRELEVANT_COLOR,
        },
    ];

    let shadowing_sphere = Sphere {
        position: Point::new(5.0, 0.0, 0.0),
        radius: 2.0,
        color: IRRELEVANT_COLOR,
        reflectiveness: IRRELEVANT_REFLECTIVENESS,
    };
    let non_shadowing_sphere = Sphere {
        position: Point::new(0.0, 15.0, 0.0),
        radius: 2.0,
        color: IRRELEVANT_COLOR,
        reflectiveness: IRRELEVANT_REFLECTIVENESS,
    };
    let objects: Vec<Box<dyn Intersectable>> =
        vec![Box::new(shadowing_sphere), Box::new(non_shadowing_sphere)];
    let surface_point = SurfacePoint {
        position: Point::new(0.0, 0.0, 0.0),
        color: IRRELEVANT_COLOR,
        normal: (LIGHTS[0].position + LIGHTS[1].position).normalize(),
        reflectiveness: IRRELEVANT_REFLECTIVENESS,
    };
    let valid_lights = reachable_lights(&objects, &LIGHTS, surface_point);
    assert_eq!(valid_lights.len(), 1);
    assert_eq!(valid_lights[0].position, LIGHTS[1].position);
}
