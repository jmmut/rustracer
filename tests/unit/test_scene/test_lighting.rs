use rustracer::scene::intersectable::Color;
use rustracer::scene::lighting::{Coloring, Light, ShadedColoring};
use rustracer::scene::{Direction, Point, SurfacePoint};

const BACKGROUND_COLOR: Color = Color {
    r: 40,
    g: 40,
    b: 40,
    a: 255,
};

const LIGHTS: [Light; 1] = [Light {
    position: Point::new(8.0, 25.0, 32.0),
    color: Color {
        r: 200,
        g: 240,
        b: 150,
        a: 255,
    },
}];

#[test]
fn illuminate() {
    let mut pixel_position = LIGHTS[0].position;
    pixel_position.y -= 5.0;
    pixel_position.x -= 1.0;
    let color = Color {
        r: 100,
        g: 100,
        b: 100,
        a: 255,
    };
    let pixel_light = ShadedColoring::compute_color(
        &SurfacePoint {
            position: pixel_position,
            color,
            normal: Direction::new(0.0, 1.0, 0.0),
            reflectiveness: 0.5,
        },
        &LIGHTS,
    );
    let pixel_dark = ShadedColoring::compute_color(
        &SurfacePoint {
            position: pixel_position,
            color,
            normal: Direction::new(1.0, 0.0, 0.0),
            reflectiveness: 0.5,
        },
        &LIGHTS,
    );
    assert_ne!(pixel_light, BACKGROUND_COLOR);
    assert_ne!(pixel_dark, BACKGROUND_COLOR);
    assert_eq!(
        pixel_light.r > pixel_dark.r,
        true,
        "expected {:?} > {:?}",
        pixel_light.r,
        pixel_dark.r
    );
}

#[test]
fn basic_reflection() {
    let red = Color {
        r: 200,
        g: 0,
        b: 0,
        a: 255,
    };
    let blue = Color {
        r: 0,
        g: 0,
        b: 200,
        a: 255,
    };
    let reflection = ShadedColoring::compute_reflection(red, blue, 0.5);
    let expected_color = Color {
        r: 100,
        g: 0,
        b: 100,
        a: 255,
    };
    assert_eq!(reflection, expected_color);
}

#[test]
fn overflowing_reflection() {
    let red = Color {
        r: 200,
        g: 200,
        b: 0,
        a: 255,
    };
    let blue = Color {
        r: 0,
        g: 200,
        b: 200,
        a: 255,
    };
    let reflection = ShadedColoring::compute_reflection(red, blue, 0.5);
    let expected_color = Color {
        r: 100,
        g: 200,
        b: 100,
        a: 255,
    };
    assert_eq!(reflection, expected_color);
}
