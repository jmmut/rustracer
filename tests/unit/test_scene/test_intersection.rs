use rustracer::scene::intersectable::*;
use rustracer::scene::sphere::*;

const BASIC_COLOR: Color = Color {
    r: 0,
    g: 0,
    b: 0,
    a: 0,
};

#[test]
fn basic_intersection() {
    let ray = Ray {
        position: Point::new(10.0, 20.0, 30.0),
        direction: Point::new(0.0, 0.0, -1.0),
    };
    let sphere = Sphere {
        position: Point::new(10.0, 20.0, 0.0),
        radius: 2.0,
        color: BASIC_COLOR,
        reflectiveness: 0.5,
    };
    let intersections = sphere.intersect(&ray);
    assert!(intersections.is_some());
    assert_eq!(
        intersections.unwrap().point.position,
        Point::new(10.0, 20.0, 2.0)
    );
}

#[test]
fn no_backwards_intersection() {
    let ray = Ray {
        position: Point::new(10.0, 20.0, 30.0),
        direction: Point::new(0.0, 0.0, -1.0),
    };
    let sphere = Sphere {
        position: Point::new(10.0, 20.0, 40.0),
        radius: 2.0,
        color: BASIC_COLOR,
        reflectiveness: 0.5,
    };
    let intersections = sphere.intersect(&ray);
    assert!(intersections.is_none());
}

#[test]
fn no_intersection_from_inside() {
    let ray = Ray {
        position: Point::new(10.0, 20.0, 30.0),
        direction: Point::new(0.0, 0.0, -1.0),
    };
    let sphere = Sphere {
        position: Point::new(10.0, 20.0, 29.0),
        radius: 2.0,
        color: BASIC_COLOR,
        reflectiveness: 0.5,
    };
    let intersections = sphere.intersect(&ray);
    assert!(intersections.is_none());
}
