use rustracer::scene::camera::Camera;
use rustracer::scene::intersectable::*;
use rustracer::scene::sampler::{PixelCenterSampler, PixelCornersSampler};
use rustracer::scene::Sampler;

#[test]
fn spawn_rays_from_camera() {
    let camera = Camera {
        position: Point::new(10.0, 20.0, 30.0),
        direction: Vector::new(0.0, 0.0, -5.0),
        half_width_right: Vector::new(2.0, 0.0, 0.0),
        half_height_up: Vector::new(0.0, 1.0, 0.0),
        pixels_width: 2,
        pixels_height: 2,
    };
    let rays = PixelCenterSampler {}.spawn_rays(camera);
    assert_eq!(rays.len(), 4);
    assert_eq!(rays[0].position, Point::new(9.0, 20.5, 25.0));
    assert_eq!(rays[0].direction, Point::new(-1.0, 0.5, -5.0));
    assert_eq!(rays[1].position, Point::new(11.0, 20.5, 25.0));
    assert_eq!(rays[1].direction, Point::new(1.0, 0.5, -5.0));
    assert_eq!(rays[3].position, Point::new(11.0, 19.5, 25.0));
    assert_eq!(rays[3].direction, Point::new(1.0, -0.5, -5.0));
}

#[test]
fn spawn_with_oversampling() {
    let camera = Camera {
        position: Point::new(10.0, 20.0, 30.0),
        direction: Vector::new(0.0, 0.0, -5.0),
        half_width_right: Vector::new(2.0, 0.0, 0.0),
        half_height_up: Vector::new(0.0, 1.0, 0.0),
        pixels_width: 2,
        pixels_height: 2,
    };
    let sampler: &dyn Sampler = &PixelCornersSampler {};
    let rays = sampler.spawn_rays(camera);
    assert_eq!(rays.len(), 9);
    assert_eq!(rays[0].position, Point::new(8.0, 21.0, 25.0));
    assert_eq!(rays[0].direction, Point::new(-2.0, 1.0, -5.0));
    assert_eq!(rays[1].position, Point::new(10.0, 21.0, 25.0));
    assert_eq!(rays[1].direction, Point::new(0.0, 1.0, -5.0));
    assert_eq!(rays[4].position, Point::new(10.0, 20.0, 25.0));
    assert_eq!(rays[4].direction, Point::new(0.0, 0.0, -5.0));
    assert_eq!(rays[8].position, Point::new(12.0, 19.0, 25.0));
    assert_eq!(rays[8].direction, Point::new(2.0, -1.0, -5.0));
}

#[test]
fn interpolate_with_oversampling() {
    let mut pixels = vec![];
    for i in 1..=4 {
        pixels.push(Color {
            r: i * 10,
            g: i * 20,
            b: i * 30,
            a: i * 40,
        });
    }
    let pixels_width = 1;
    let pixels_height = 1;
    let sampler: &dyn Sampler = &PixelCornersSampler {};
    let rays = sampler.interpolate(pixels, pixels_width, pixels_height);

    assert_eq!(rays.len(), 1);
    assert_eq!(
        rays[0],
        Color {
            r: 25,
            g: 50,
            b: 75,
            a: 100
        }
    );
}
