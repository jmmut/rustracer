use rustracer::out::save_to_png;
use rustracer::scene::intersectable::Color;
use rustracer::scene::lighting::Light;
use rustracer::scene::sampler::PixelCenterSampler;
use rustracer::scene::{
    render, Camera, Intersectable, Point, Scene, SceneLighting, Sphere, Vector,
};

#[test]
#[ignore]
fn spheres() {
    let camera = Camera {
        position: Point::new(5.0, 20.0, 30.0),
        direction: Vector::new(0.0, 0.0, -5.0),
        half_width_right: Vector::new(1.0, 0.0, 0.0),
        half_height_up: Vector::new(0.0, 1.0, 0.0),
        pixels_width: 1000,
        pixels_height: 1000,
    };

    let scene = Scene {
        camera,
        objects: create_scene(),
        lighting: create_lights(),
        sampler: Box::new(PixelCenterSampler {}),
    };
    let frame = render(scene);

    save_to_png(frame, "tests/resources/spheres.png");
}

fn create_scene() -> Vec<Box<dyn Intersectable>> {
    vec![
        Box::new(Sphere {
            position: Point::new(10.0, 20.0, 0.0),
            radius: 3.0,
            color: Color {
                r: 40,
                g: 100,
                b: 250,
                a: 255,
            },
            reflectiveness: 0.5,
        }),
        Box::new(Sphere {
            position: Point::new(0.0, 20.0, 0.0),
            radius: 3.0,
            color: Color {
                r: 250,
                g: 100,
                b: 40,
                a: 255,
            },
            reflectiveness: 0.5,
        }),
    ]
}

fn create_lights() -> SceneLighting {
    SceneLighting {
        lights: vec![Light {
            position: Point::new(8.0, 25.0, 5.0),
            color: Color {
                r: 200,
                g: 240,
                b: 150,
                a: 255,
            },
        }],
        background_color: Color {
            r: 40,
            g: 40,
            b: 40,
            a: 255,
        },
        reflection_iterations: 4,
    }
}
