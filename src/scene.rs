pub use camera::Camera;
pub use intersectable::Color;
pub use intersectable::{Direction, Intersectable, Point, Ray, SurfacePoint, Vector};
pub use lighting::{Light, SceneLighting};
use raytrace::intersect_rays_with_objects;
pub use sampler::{PixelCenterSampler, PixelCornersSampler, Sampler};
pub use sphere::Sphere;

pub mod camera;
pub mod intersectable;
pub mod lighting;
pub mod raytrace;
pub mod sampler;
pub mod sphere;

trait Renderer {
    type UsedSampler;
    fn render() -> Frame;
}

pub struct Scene {
    pub camera: Camera,
    pub objects: Vec<Box<dyn Intersectable>>,
    pub lighting: SceneLighting,
    pub sampler: Box<dyn Sampler>,
}

pub struct Frame {
    pub pixels: Vec<Color>,
    pub width: u32,
    pub height: u32,
}

pub fn render(scene: Scene) -> Frame {
    let width = scene.camera.pixels_width;
    let height = scene.camera.pixels_height;
    let rays = scene.sampler.spawn_rays(scene.camera);
    let raw_pixels = intersect_rays_with_objects(rays, scene.objects, scene.lighting);
    let pixels = scene.sampler.interpolate(raw_pixels, width, height);
    Frame {
        pixels,
        width,
        height,
    }
}
