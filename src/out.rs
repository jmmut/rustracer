use crate::scene::intersectable::Color;
use crate::scene::Frame;

pub fn save_to_png(data: Frame, path_str: &str) {
    use std::path::Path;
    let path = Path::new(path_str);
    let file = &std::fs::File::create(path).unwrap();
    save_to_png_file(data, file);
}

pub fn save_to_png_file(data: Frame, file: &std::fs::File) {
    let mut components = Vec::with_capacity(data.pixels.len() * core::mem::size_of::<Color>());
    for color in data.pixels {
        components.push(color.r);
        components.push(color.g);
        components.push(color.b);
        components.push(color.a);
    }
    save_array_to_png_file(&components, file, data.width, data.height);
}

pub fn save_array_to_png_file(data: &[u8], file: &std::fs::File, width: u32, height: u32) {
    use std::io::BufWriter;

    let ref mut w = BufWriter::new(file);

    let mut encoder = png::Encoder::new(w, width, height);
    encoder.set_color(png::ColorType::Rgba);
    encoder.set_depth(png::BitDepth::Eight);
    encoder.set_trns(vec![0xFFu8, 0xFFu8, 0xFFu8, 0xFFu8]);
    encoder.set_source_gamma(png::ScaledFloat::from_scaled(45455)); // 1.0 / 2.2, scaled by 100000
    encoder.set_source_gamma(png::ScaledFloat::new(1.0 / 2.2)); // 1.0 / 2.2, unscaled, but rounded
    let source_chromaticities = png::SourceChromaticities::new(
        // Using unscaled instantiation here
        (0.31270, 0.32900),
        (0.64000, 0.33000),
        (0.30000, 0.60000),
        (0.15000, 0.06000),
    );
    encoder.set_source_chromaticities(source_chromaticities);
    let mut writer = encoder.write_header().unwrap();
    writer.write_image_data(&data).unwrap(); // Save
}
