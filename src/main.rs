use cgmath::InnerSpace;

use rustracer::out::save_to_png;
use rustracer::scene::intersectable::Color;
use rustracer::scene::lighting::Light;
use rustracer::scene::sampler::PixelCenterSampler;
use rustracer::scene::{
    render, Camera, Intersectable, Point, Scene, SceneLighting, Sphere, Vector,
};

fn main() {
    let scene = create_scene();
    let frame = render(scene);
    save_to_png(frame, "resources/main.png");
}

fn create_scene() -> Scene {
    let scene = Scene {
        camera: create_camera(),
        objects: create_objects(),
        lighting: create_lights(),
        sampler: Box::new(PixelCenterSampler {}),
    };
    scene
}

fn create_camera() -> Camera {
    let mut direction = Vector::new(-4.0, 0.0, -5.0);
    direction = direction.normalize();

    let half_height_up = Vector::new(0.0, 1.0, 0.0);
    let half_width_right = direction.cross(half_height_up);
    // let pixels_width = 1600;
    // let pixels_height = 900;
    let pixels_width = 1000;
    let pixels_height = 1000;
    let half_width_right = half_width_right * ((pixels_width as f64) / (pixels_height as f64));
    let camera = Camera {
        position: Point::new(21.0, 0.0, 40.0),
        direction,
        half_width_right,
        half_height_up,
        pixels_width,
        pixels_height,
    };
    camera
}

fn create_objects() -> Vec<Box<dyn Intersectable>> {
    let wall_color = Color {
        r: 250,
        g: 200,
        b: 150,
        a: 255,
    };
    let floor_color = Color {
        r: 60,
        g: 100,
        b: 160,
        a: 255,
    };
    let ceiling_color = Color {
        r: 250,
        g: 250,
        b: 250,
        a: 255,
    };
    vec![
        Box::new(Sphere {
            position: Point::new(0.0, 0.0, 0.0),
            radius: 5.0,
            color: Color {
                r: 40,
                g: 100,
                b: 250,
                a: 255,
            },
            reflectiveness: 0.0,
        }),
        Box::new(Sphere {
            position: Point::new(-330.0, 0.0, 0.0),
            radius: 300.0,
            color: wall_color,
            reflectiveness: 0.0,
        }),
        Box::new(Sphere {
            position: Point::new(330.0, 0.0, 0.0),
            radius: 300.0,
            color: wall_color,
            reflectiveness: 0.0,
        }),
        Box::new(Sphere {
            position: Point::new(0.0, 0.0, -350.0),
            radius: 300.0,
            color: wall_color,
            reflectiveness: 0.0,
        }),
        Box::new(Sphere {
            position: Point::new(0.0, 0.0, 350.0),
            radius: 300.0,
            color: wall_color,
            reflectiveness: 0.0,
        }),
        Box::new(Sphere {
            position: Point::new(0.0, -320.0, 0.0),
            radius: 300.0,
            color: floor_color,
            reflectiveness: 0.0,
        }),
        Box::new(Sphere {
            position: Point::new(0.0, 320.0, 0.0),
            radius: 300.0,
            color: ceiling_color,
            reflectiveness: 0.0,
        }),
    ]
}

fn create_lights() -> SceneLighting {
    SceneLighting {
        lights: vec![
            Light {
                position: Point::new(40.0, 5.0, 60.0),
                color: Color {
                    r: 240,
                    g: 220,
                    b: 160,
                    a: 255,
                },
            },
            Light {
                position: Point::new(8.0, 0.0, 0.0),
                color: Color {
                    r: 60,
                    g: 60,
                    b: 60,
                    a: 255,
                },
            },
        ],
        background_color: Color {
            r: 40,
            g: 40,
            b: 40,
            a: 255,
        },
        reflection_iterations: 4,
    }
}
