use crate::scene::intersectable::{
    Color, Intersectable, IntersectionPoint, Point, Ray, SurfacePoint,
};
use cgmath::InnerSpace;

pub struct Sphere {
    pub position: Point,
    pub radius: f64,
    pub color: Color,
    pub reflectiveness: f32,
}

impl Intersectable for Sphere {
    fn intersect_if_closer_than(&self, ray: &Ray, distance: f64) -> Option<IntersectionPoint> {
        let ray_point_to_sphere = self.position - ray.position;
        if ray_point_to_sphere.magnitude() <= self.radius {
            // ray origin inside sphere
            Option::None
        } else {
            let ray_unit_dir = ray.direction.normalize();
            let distance_from_ray_to_closest_point = cgmath::dot(ray_point_to_sphere, ray_unit_dir);
            if distance_from_ray_to_closest_point < 0.0 {
                // intersection behind ray start
                Option::None
            } else {
                let closest_point = ray_unit_dir * distance_from_ray_to_closest_point;
                let sphere_to_closest_point = closest_point - ray_point_to_sphere;
                let distance_from_sphere_to_closest_point = sphere_to_closest_point.magnitude();
                if distance_from_sphere_to_closest_point > self.radius {
                    Option::None
                } else {
                    let distance_from_closest_point_to_surface = f64::sqrt(
                        self.radius * self.radius
                            - distance_from_sphere_to_closest_point
                                * distance_from_sphere_to_closest_point,
                    );
                    // let farther_distance =
                    //     distance_from_ray_to_closest_point + distance_from_closest_point_to_surface;
                    let closer_distance =
                        distance_from_ray_to_closest_point - distance_from_closest_point_to_surface;

                    if closer_distance < distance {
                        let closer_point = ray_unit_dir * closer_distance + ray.position;
                        Option::Some(IntersectionPoint {
                            point: SurfacePoint {
                                position: closer_point,
                                color: self.color,
                                normal: (closer_point - self.position).normalize(),
                                reflectiveness: self.reflectiveness,
                            },
                            distance: closer_distance,
                        })
                    } else {
                        Option::None
                    }
                }
            }
        }
    }
}
