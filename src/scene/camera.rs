use crate::scene::intersectable::{Point, Vector};

pub struct Camera {
    pub position: Point,
    pub direction: Vector,
    pub half_width_right: Vector,
    pub half_height_up: Vector,
    pub pixels_width: u32,
    pub pixels_height: u32,
}
