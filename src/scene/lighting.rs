use cgmath::num_traits::clamp_min;
use cgmath::{dot, InnerSpace};

use crate::scene::intersectable::Color;
use crate::scene::{Direction, Point, SurfacePoint};

#[derive(Copy, Clone)]
pub struct Light {
    pub position: Point,
    // pub direction: Direction,
    pub color: Color,
}

pub struct SceneLighting {
    pub lights: Vec<Light>,
    pub background_color: Color,
    pub reflection_iterations: i32,
}

pub trait Coloring {
    fn compute_color(surface_point: &SurfacePoint, lights: &[Light]) -> Color;
    fn compute_reflection(
        surface_color: Color,
        bounced_ray_color: Color,
        reflectiveness: f32,
    ) -> Color;
}

pub struct ShadedColoring;

impl Coloring for ShadedColoring {
    fn compute_color(surface_point: &SurfacePoint, lights: &[Light]) -> Color {
        let accumulated_light = compute_light(lights, surface_point.position, surface_point.normal);
        let accumulated_color = desaturate(surface_point.color, accumulated_light);
        accumulated_color
    }

    fn compute_reflection(
        surface_color: Color,
        bounced_ray_color: Color,
        reflectiveness: f32,
    ) -> Color {
        surface_color * (1.0 - reflectiveness) + bounced_ray_color * reflectiveness
    }
}

fn compute_light(lights: &[Light], surface_position: Point, surface_normal: Direction) -> Color {
    let mut dimmed_lights = Vec::new();

    for light in lights {
        dimmed_lights.push(dim(light, surface_normal, surface_position));
    }
    let mut accum_color = Color {
        r: 0,
        g: 0,
        b: 0,
        a: 255,
    };
    for light in dimmed_lights {
        accum_color = accum_color + light;
    }
    accum_color
}

pub fn dim(light: &Light, surface_normal: Direction, surface: Point) -> Color {
    let surface_to_light = light.position - surface;
    let surface_to_light_normalized = (surface_to_light).normalize();
    let cos_angle = dot(surface_normal, surface_to_light_normalized);
    let mut intensity = 1.0;
    let distance = surface_to_light.magnitude();
    let distance_coef = 10_000.0;
    if distance > 1.0 {
        intensity *= distance_coef / (distance * distance - 1.0 + distance_coef);
        // intensity /= (distance * distance - 1.0) / distance_coef + 1.0;
    }
    let light_coef = clamp_min(cos_angle * intensity, 0.0);
    Color {
        r: (light.color.r as f64 * light_coef) as u8,
        g: (light.color.g as f64 * light_coef) as u8,
        b: (light.color.b as f64 * light_coef) as u8,
        a: light.color.a,
    }
    //light.color * (light_coef as f32)
}

pub fn desaturate(color: Color, light: Color) -> Color {
    Color {
        r: (color.r as u32 * light.r as u32 / 255) as u8,
        g: (color.g as u32 * light.g as u32 / 255) as u8,
        b: (color.b as u32 * light.b as u32 / 255) as u8,
        a: color.a,
    }
}
