use cgmath::{dot, InnerSpace};

use crate::scene::lighting::{Coloring, ShadedColoring};
use crate::scene::{Color, Intersectable, Light, Ray, SceneLighting, SurfacePoint};

pub fn intersect_rays_with_objects(
    rays: Vec<Ray>,
    objects: Vec<Box<dyn Intersectable>>,
    lighting: SceneLighting,
) -> Vec<Color> {
    let mut pixels = Vec::<Color>::with_capacity(rays.len());
    for ray in rays {
        let pixel = intersect_ray_with_objects::<ShadedColoring>(
            &objects,
            &lighting.lights,
            lighting.background_color,
            &ray,
            lighting.reflection_iterations,
        );
        pixels.push(pixel);
    }
    pixels
}

pub fn intersect_ray_with_objects<C: Coloring>(
    objects: &[Box<dyn Intersectable>],
    lights: &[Light],
    background_color: Color,
    ray: &Ray,
    remaining_bounces: i32,
) -> Color {
    let closest_intersection = closest_intersection(objects, &ray);
    if closest_intersection.is_none() {
        background_color
    } else {
        let surface_point = closest_intersection.unwrap();
        let filtered_lights = reachable_lights(objects, lights, surface_point);
        let surface_color = C::compute_color(&closest_intersection.unwrap(), &filtered_lights);
        if remaining_bounces <= 0 {
            surface_color
        } else {
            let bounced_ray = bounce_ray(ray, surface_point);
            let reflection = intersect_ray_with_objects::<C>(
                objects,
                lights,
                background_color,
                &bounced_ray,
                remaining_bounces - 1,
            );
            C::compute_reflection(surface_color, reflection, surface_point.reflectiveness)
        }
    }
}

fn closest_intersection(objects: &[Box<dyn Intersectable>], ray: &Ray) -> Option<SurfacePoint> {
    let mut closest_intersection = Option::<SurfacePoint>::None;
    let mut closest_distance = f64::MAX;
    for object in objects {
        let new_closest = object.intersect_if_closer_than(&ray, closest_distance);
        if new_closest.is_some() {
            let point = new_closest.unwrap();
            closest_distance = point.distance;
            closest_intersection = Option::Some(point.point);
        }
    }
    closest_intersection
}

pub fn reachable_lights(
    objects: &[Box<dyn Intersectable>],
    lights: &[Light],
    surface_point: SurfacePoint,
) -> Vec<Light> {
    let mut non_occluded_lights: Vec<Light> = vec![];
    for light in lights {
        let surface_to_light = light.position - surface_point.position;
        let ray = Ray {
            direction: surface_to_light.normalize(),
            position: surface_point.position,
        };
        let closest_surface_point = closest_intersection(objects, &ray);
        if closest_surface_point.is_none() {
            non_occluded_lights.push(light.clone());
        } else {
            let surface_to_intersection =
                closest_surface_point.unwrap().position - surface_point.position;
            if surface_to_intersection.magnitude() > surface_to_light.magnitude() {
                non_occluded_lights.push(light.clone());
            }
        }
    }
    non_occluded_lights
}
pub fn bounce_ray(ray: &Ray, surface_point: SurfacePoint) -> Ray {
    let proyection = dot(ray.direction, surface_point.normal) * surface_point.normal;
    let bounced_ray = Ray {
        direction: ray.direction - 2.0 * proyection,
        position: surface_point.position,
    };
    bounced_ray
}
