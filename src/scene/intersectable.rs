use cgmath::num_traits::clamp;
use std::ops::{Add, Mul};

pub trait Intersectable {
    fn intersect_if_closer_than(&self, ray: &Ray, distance: f64) -> Option<IntersectionPoint>;
    fn intersect(&self, ray: &Ray) -> Option<IntersectionPoint> {
        self.intersect_if_closer_than(ray, f64::MAX)
    }
}

pub struct Ray {
    pub position: Point,
    pub direction: Direction,
}

#[derive(Copy, Clone)]
pub struct SurfacePoint {
    pub position: Point,
    pub color: Color,
    pub normal: Direction,
    pub reflectiveness: f32,
}

pub struct IntersectionPoint {
    pub point: SurfacePoint,

    /// Distance from the ray origin position
    pub distance: f64,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Add for Color {
    type Output = Color;

    fn add(self, rhs: Self) -> Self::Output {
        Color {
            r: clamp(self.r as u32 + rhs.r as u32, 0, 255) as u8,
            g: clamp(self.g as u32 + rhs.g as u32, 0, 255) as u8,
            b: clamp(self.b as u32 + rhs.b as u32, 0, 255) as u8,
            a: self.a,
        }
    }
}

impl Mul<f32> for Color {
    type Output = Color;

    fn mul(self, rhs: f32) -> Self::Output {
        Color {
            r: f32::round(clamp(self.r as f32 * rhs, 0.0, 255.0)) as u8,
            g: f32::round(clamp(self.g as f32 * rhs, 0.0, 255.0)) as u8,
            b: f32::round(clamp(self.b as f32 * rhs, 0.0, 255.0)) as u8,
            a: self.a,
        }
    }
}
/// unitary direction
pub type Direction = cgmath::Vector3<f64>;

/// point respect to the origin
pub type Point = cgmath::Vector3<f64>;

/// difference between 2 points
pub type Vector = cgmath::Vector3<f64>;
