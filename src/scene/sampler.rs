use crate::scene::{Camera, Color, Ray};

pub trait Sampler {
    fn spawn_rays(&self, camera: Camera) -> Vec<Ray>;
    fn interpolate(&self, pixels: Vec<Color>, pixels_width: u32, pixels_height: u32) -> Vec<Color>;
}

pub struct PixelCenterSampler;
pub struct PixelCornersSampler;

impl Sampler for PixelCenterSampler {
    fn spawn_rays(&self, camera: Camera) -> Vec<Ray> {
        let mut rays = Vec::with_capacity((camera.pixels_width * camera.pixels_height) as usize);
        let half_pixel_width_right = camera.half_width_right / (camera.pixels_width as f64);
        let half_pixel_height_up = camera.half_height_up / (camera.pixels_height as f64);
        let top_left =
            camera.position + camera.direction + camera.half_height_up - camera.half_width_right;
        let first_pixel = top_left - half_pixel_height_up + half_pixel_width_right;

        for i_h in 0..camera.pixels_height {
            let line = -(i_h as f64) * half_pixel_height_up * 2.0;
            for i_w in 0..camera.pixels_width {
                let position = first_pixel + half_pixel_width_right * (i_w as f64) * 2.0 + line;
                rays.push(Ray {
                    position,
                    direction: position - camera.position,
                });
            }
        }
        rays
    }

    fn interpolate(
        &self,
        pixels: Vec<Color>,
        _pixels_width: u32,
        _pixels_height: u32,
    ) -> Vec<Color> {
        pixels
    }
}

impl Sampler for PixelCornersSampler {
    fn spawn_rays(&self, camera: Camera) -> Vec<Ray> {
        let rays_width = camera.pixels_width + 1;
        let rays_height = camera.pixels_height + 1;
        let mut rays = Vec::with_capacity((rays_width * rays_height) as usize);
        let pixel_width_right = camera.half_width_right * 2.0 / (camera.pixels_width as f64);
        let pixel_height_up = camera.half_height_up * 2.0 / (camera.pixels_height as f64);
        let top_left =
            camera.position + camera.direction + camera.half_height_up - camera.half_width_right;

        for i_h in 0..rays_height {
            let line = -(i_h as f64) * pixel_height_up;
            for i_w in 0..rays_width {
                let position = top_left + pixel_width_right * (i_w as f64) + line;
                rays.push(Ray {
                    position,
                    direction: position - camera.position,
                });
            }
        }
        rays
    }

    fn interpolate(&self, pixels: Vec<Color>, pixels_width: u32, pixels_height: u32) -> Vec<Color> {
        let mut center_pixels = vec![];
        for center_h in 0..pixels_height {
            for center_w in 0..pixels_width {
                let mut corners = vec![];
                for corner_h in 0..=1 {
                    for corner_w in 0..=1 {
                        corners.push(
                            pixels[Self::get_index(
                                pixels_width,
                                center_w + corner_w,
                                center_h + corner_h,
                            )],
                        );
                    }
                }
                let color = Self::average(&corners);
                center_pixels.push(color);
            }
        }
        center_pixels
    }
}

impl PixelCornersSampler {
    fn average(corners: &[Color]) -> Color {
        let mut r: u32 = 0;
        let mut g: u32 = 0;
        let mut b: u32 = 0;
        let mut a: u32 = 0;
        for corner in corners {
            r += corner.r as u32;
            g += corner.g as u32;
            b += corner.b as u32;
            a += corner.a as u32;
        }
        let color = Color {
            r: (r / corners.len() as u32) as u8,
            g: (g / corners.len() as u32) as u8,
            b: (b / corners.len() as u32) as u8,
            a: (a / corners.len() as u32) as u8,
        };
        color
    }
    fn get_index(pixel_width: u32, pixel_width_index: u32, pixel_height_index: u32) -> usize {
        (pixel_height_index * (pixel_width + 1) + pixel_width_index) as usize
    }
}
