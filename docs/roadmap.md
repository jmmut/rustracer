

Roadmap:

- [ ] different objects
  - [x] spheres
  - [ ] ...
- [ ] reflections
  - [ ] with other objects
  - [ ] with lights
- [ ] shader-like textures
- [ ] interactive
  - [ ] hierarchical sampling for real-time rendering
